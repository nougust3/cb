const OPTIONS = {
  email: "root@root.ru"
};

let isOpen = false;

function createBtn() {
  const btn = document.createElement("button");

  btn.style.position = "absolute";
  btn.style.top = `${window.innerHeight - 100}px`;
  btn.style.right = "100px";
  btn.style.width = "56px";
  btn.style.height = "56px";

  btn.addEventListener("click", () => {
    if (isOpen) {
      closeModal();
    } else {
      createModal();
      document.getElementById("send_btn").addEventListener("click", send);
      getService();
    }
  });

  document.getElementsByTagName("body")[0].appendChild(btn);
}

function closeModal() {
  isOpen = false;
  document.getElementsByClassName("modal")[0].remove();
}

function createModal() {
  isOpen = true;
  const modal = document.createElement("div");
  const modalHtml =
    '<div id="services"></div>' +
    '<input type="email" name="email" id="email_edit" placeholder="Эл. почта"><br>' +
    '<input type="text" name="name" id="name_edit" placeholder="Имя"><br>' +
    '<input type="text" name="comment" id="comment_edit" placeholder="Сообщение"><br>' +
    '<button id="send_btn">Заказать</button></div>';

  modal.className = "modal";
  modal.innerHTML = modalHtml;
  modal.style.position = "absolute";
  modal.style.top = `${window.innerHeight - 356}px`;
  modal.style.right = "100px";
  modal.style.width = "456px";
  modal.style.height = "256px";
  modal.style.background = "#fff";
  modal.style.color = "#222";

  document.body.appendChild(modal);
}

function getSelectedService() {
  const services = document.getElementsByName("service");

  for (let i = 0; i < services.length; i++) {
    if (services[i].checked) {
      return services[i].value;
    }
  }

  return null;
}

function getData() {
  const data = {};

  data.service = getSelectedService();
  data.customer = {
    name: document.getElementById("name_edit").value,
    phone: "",
    email: document.getElementById("email_edit").value,
    comment: document.getElementById("comment_edit").value
  };
  data.owner = OPTIONS.email;
  data.time = 1;

  return JSON.stringify(data);
}

function populateServices(services) {
  let servicesList = "";

  for (let i = 0; i < services.length; i++) {
    servicesList += `<input type="radio" value="${
      services[i].id
    }" id="${i}" name="service"><label for="${i}">${services[i].name}</label>`;
  }

  document.getElementById("services").innerHTML = servicesList;
}

function getService() {
  const url = "http://localhost:8081/service/all";
  const options = {
    body: JSON.stringify({ email: OPTIONS.email }),
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  };

  fetch(url, options)
    .then(response => response.json())
    .then(response => {
      if (response.success) {
        populateServices(response.services);
      }
    })
    .catch(error => console.log(error));
}

function send() {
  const url = "/task/create";
  const options = {
    body: getData(),
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  };

  fetch(url, options)
    .then(response => response.json())
    .then(json => {
      if (json.status) {
        closeModal();
      }
    })
    .catch(error => console.log(JSON.stringify(error)));
}

(function init() {
  createBtn();
})();
