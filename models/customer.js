const mongoose = require("mongoose");

const customerSchema = mongoose.Schema({
  id: String,
  email: String,
  number: String,
  name: String,
  info: String
});

module.exports = mongoose.model("Customer", customerSchema);
