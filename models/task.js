const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
  id: String,
  customer: String,
  service: String,
  owner: String,
  time: Number,
  comment: String
});

module.exports = mongoose.model('Task', taskSchema);
