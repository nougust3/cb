const mongoose = require('mongoose');

const serviceSchema = mongoose.Schema({
  id: String,
  owner: String,
  name: String,
  description: String,
  price: Number
});

module.exports = mongoose.model('Service', serviceSchema);
