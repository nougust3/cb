const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");

const userSchema = mongoose.Schema({
  email: String,
  password: String,
  workTime: {
    start: Number,
    end: Number
  },
  type: Boolean
});

userSchema.methods.generateHash = password =>
  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

userSchema.methods.validPassword = (user, password) =>
  new Promise((resolve, reject) => {
    const isEqual = password === user.password;

    if (isEqual) {
      resolve(user);
    } else {
      reject({ code: 4, msg: "wrong password" });
    }
  });

module.exports = mongoose.model("User", userSchema);
