define({
  "name": "Customer Base API Documentation",
  "title": "Customer Base API Documentation",
  "url": "http://localhost:8081/v1",
  "template": {
    "forceLanguage": "en"
  },
  "version": "0.0.0",
  "description": "",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-04-18T09:27:28.112Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
