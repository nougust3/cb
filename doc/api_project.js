define({
  "name": "Express API Sample Documentation",
  "title": "Express API Sample Documentation",
  "description": "Sample API ready to be used in different client app boilerplates and playgrounds.",
  "url": "http://localhost:8081/v1",
  "template": {
    "forceLanguage": "en"
  },
  "version": "0.0.0",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-04-18T08:35:48.143Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
