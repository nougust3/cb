define({ "api": [
  {
    "type": "post",
    "url": "/service/get",
    "title": "Request Service information",
    "name": "GetService",
    "group": "Service",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>request result status true.</p>"
          },
          {
            "group": "Success 200",
            "type": "Service",
            "optional": false,
            "field": "service",
            "description": "<p>Service object.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>request result status true.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/_apidoc.js",
    "groupTitle": "Service"
  },
  {
    "type": "post",
    "url": "/service/get",
    "title": "Request Service information",
    "name": "GetService",
    "group": "Service",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>request result status true.</p>"
          },
          {
            "group": "Success 200",
            "type": "Service",
            "optional": false,
            "field": "service",
            "description": "<p>Service object.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>request result status true.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/service.router.js",
    "groupTitle": "Service"
  }
] });
