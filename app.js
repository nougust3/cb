const express = require("express");
const session = require("express-session");
const path = require("path");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const mongo = require("mongoose");
const passport = require("passport");
const flash = require("connect-flash");
const port = process.env.PORT || 8081;

require("./config/passport")(passport);

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "client")));
app.use(express.static(path.join(__dirname, "demo")));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(session({ secret: "secret", saveUninitialized: true, resave: false }));
app.use(passport.initialize());
app.use(passport.session());
mongo.connect(
  "mongodb://root:root@ds247759.mlab.com:47759/cb",
  (err, database) => {
    if (err) {
      return console.log(err);
    }
  }
);
app.use(flash());

app.set("view engine", "dot");

require("./routes/script")(app);
require("./routes/user.router")(app, passport);
require("./routes/task.router")(app, passport);
require("./routes/service.router")(app, passport);
require("./routes/customer.router")(app, passport);

app.use((req, res, next) => {
  const err = new Error("Not Found");

  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  res.status(err.status || 500);
  res.render("error");
});

app.listen(port);

module.exports = app;
