const Controller = require("../controllers/user.controller");
const token = require("jsonwebtoken");

module.exports = (app, passport) => {
  function getUser(req) {
    return {
      id: req.body.id,
      email: req.body.email,
      name: req.body.name,
      workTime: req.body.workTime
    };
  }

  function createToken(user) {
    return new Promise((resolve, reject) => {
      token.sign(
        user.toJSON(),
        "secret",
        { algorithm: "RS256" },
        (err, res) => {
          if (err) {
            reject(err);
          } else {
            resolve(res);
          }
        }
      );
    });
  }

  app.post("/user/signup", (req, res) => {
    Controller.create({
      email: req.body.email,
      password: req.body.password,
      name: req.body.name
    })
      .then(user => createToken(user))
      .then(token =>
        res.json({
          success: true,
          token: token.sign(user.toJSON(), "secret")
        })
      )
      .catch(error =>
        res.json({
          success: false,
          code: error.code,
          msg: error.msg
        })
      );
  });

  app.post("/user/signin", (req, res) => {
    Controller.get(req.body.email)
      .then(user => user.validPassword(user, req.body.password))
      .then(user =>
        res.json({
          success: true,
          token: token.sign(user.toJSON(), "secret")
        })
      )
      .catch(err =>
        res.json({
          success: false,
          code: err.code,
          msg: err.msg
        })
      );
  });

  app.post("/user/get", (req, res) => {
    Controller.get(req.body.email)
      .then(user => res.json({ success: true, user }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  app.post("/user/update", (req, res) => {
    Controller.update(getUser(req))
      .then(() => res.json({ success: true }))
      .catch(err => res.json({ success: false, msg: err }));
  });
};
