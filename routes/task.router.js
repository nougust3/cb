const TaskController = require("../controllers/task.controller");

module.exports = (app, passport) => {
  /**
   * @api {post} /task/create Create new Task
   * @apiName CreateTask
   * @apiGroup Task
   *
   * @apiParam {Customer} customer Customer object with email and/or phone values.
   * @apiParam {String} service Service id.
   * @apiParam {String} owner Service owner email
   * @apiParam {Number} time Task time in milliseconds.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {String} taskId Unique Task id.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/task/create", (req, res) => {
    TaskController.create(req.body)
      .then(taskId =>
        res.json({
          success: true,
          id: taskId
        })
      )
      .catch(err => {
        res.json({
          success: false,
          msg: err
        });
      });
  });

  /**
   * @api {post} /task/all Get all Tasks for user
   * @apiName AllTask
   * @apiGroup Task
   *
   * @apiParam {String} email User email.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {Task[]} tasks Tasks objects array.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/task/all", (req, res) => {
    TaskController.getAll(req.body.email)
      .then(tasks =>
        res.json({
          success: true,
          tasks
        })
      )
      .catch(err =>
        res.json({
          success: false,
          msg: err
        })
      );
  });

  /**
   * @api {post} /task/remove Remove Task
   * @apiName RemoveTask
   * @apiGroup Task
   *
   * @apiParam {String} id Id of Task for remove.
   *
   * @apiSuccess {Boolean} success request result status true.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/task/remove", (req, res) => {
    TaskController.remove(req.body.id)
      .then(() => {
        res.json({
          success: true
        });
      })
      .catch(err => {
        res.json({
          success: false,
          msg: err
        });
      });
  });

  /**
   * @api {post} /task/update Update Task information
   * @apiName UpdateTask
   * @apiGroup Task
   *
   * @apiParam {String} id Id of Task for remove.
   * @apiParam {Number} time Task time.
   * @apiParam {String} comment Task comment.
   * @apiParam {String} customer Customer id.
   * @apiParam {String} service Service id.
   *
   * @apiSuccess {Boolean} success request result status true.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/task/update", (req, res) => {
    TaskController.update(req.body)
      .then(() => {
        res.json({
          success: true
        });
      })
      .catch(err => {
        res.json({
          success: false,
          msg: err
        });
      });
  });
};
