const Controller = require("../controllers/customer.controller");

module.exports = (app, passport) => {
  app.post("/customer/all", (req, res) => {
    Controller.getAll(req.body.email)
      .then(customers => res.json({ success: true, customers }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  app.post("/customer/create", (req, res) => {
    Controller.create(req.body.customer)
      .then(customer =>
        res.json({
          success: true,
          customer
        })
      )
      .catch(err => res.json({ success: false, msg: err }));
  });

  app.post("/customer/update", (req, res) => {
    Controller.update(req.body)
      .then(customer =>
        res.json({
          success: true,
          customer
        })
      )
      .catch(err => res.json({ success: false, msg: err }));
  });
};
