const Controller = require("../controllers/service.controller");

module.exports = (app, passport) => {
  function getService(req) {
    return {
      id: req.body.id,
      owner: req.body.owner,
      name: req.body.name,
      description: req.body.description,
      price: req.body.price
    };
  }

  /**
   * @api {post} /service/get Request Service information
   * @apiName GetService
   * @apiGroup Service
   *
   * @apiParam {String} id Service unique ID.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {Service} service Service object.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/service/get", (req, res) => {
    Controller.get(req.body.id)
      .then(service => res.json({ success: true, service }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  /**
   * @api {post} /service/all Request all Services information
   * @apiName GetServices
   * @apiGroup Service
   *
   * @apiParam {String} email email of services owner.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {Service[]} services Array of Service objects.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/service/all", (req, res) => {
    Controller.getAll(req.body.email)
      .then(services => res.json({ success: true, services }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  /**
   * @api {post} /service/create Create new Service
   * @apiName CreateServices
   * @apiGroup Service
   *
   * @apiParam {String} name Service name.
   * @apiParam {String} owner Service owner.
   * @apiParam {String} description Service description.
   * @apiParam {Number} price Service price.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {Service[]} services Created Service object.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/service/create", (req, res) => {
    Controller.create(req.body.email, req.body.service)
      .then(service => res.json({ success: true, service }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  /**
   * @api {post} /service/remove Remove Service
   * @apiName RemoveServices
   * @apiGroup Service
   *
   * @apiParam {String} id Unique Service id.
   * @apiParam {String} name Service name.
   * @apiParam {String} owner Service owner.
   * @apiParam {String} description Service description.
   * @apiParam {Number} price Service price.
   *
   * @apiSuccess {Boolean} success request result status true.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/service/remove", (req, res) => {
    Controller.remove(req.body.id)
      .then(() => res.json({ success: true }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  /**
   * @api {post} /service/update Update Service information
   * @apiName UpdateServices
   * @apiGroup Service
   *
   * @apiParam {Service} service Updated Service object.
   *
   * @apiSuccess {Boolean} success request result status true.
   * @apiSuccess {Service} service Updated Service objects.
   *
   * @apiError {Boolean} success request result status false.
   * @apiError {String} msg error message.
   */
  app.post("/service/update", (req, res) => {
    Controller.update(getService(req))
      .then(service => res.json({ success: true, service: service }))
      .catch(err => res.json({ success: false, msg: err }));
  });

  function auth(req, res, next) {
    const token = req.headers["Authorization"];
    if (token) {
      req.token = token;
      next();
    } else {
      res.send(403);
    }
  }
};
