const Mongoose = require("mongoose");
const User = Mongoose.model("User");
const uuidV1 = require("uuid/v1");

function generateId() {
  return uuidV1();
}

exports.getById = id =>
  new Promise((resolve, reject) => {
    User.findOne({ id }, (err, user) => {
      if (!err) {
        if (!user) {
          reject({ code: 3, msg: "User not found." });
        } else {
          resolve(user);
        }
      } else {
        reject({ code: 0, msg: err });
      }
    });
  });

exports.get = email =>
  new Promise((resolve, reject) => {
    User.findOne({ email }, (err, user) => {
      if (!err) {
        if (!user) {
          reject({ code: 3, msg: "User not found." });
        } else {
          resolve(user);
        }
      } else {
        reject({ code: 0, msg: err });
      }
    });
  });

exports.create = user =>
  new Promise((resolve, reject) => {
    User.findOne({ email: user.email }, (err, user1) => {
      if (!err) {
        if (!user1) {
          const newUser = new User();

          newUser.id = generateId();
          newUser.email = user.email;
          newUser.name = user.name;
          newUser.password = user.password;
          newUser.workTime = user.workTime;
          newUser.save(err1 => {
            if (err1) {
              reject({ code: 1, msg: err1 });
            }
            resolve(newUser);
          });
        } else {
          reject({ code: 2, msg: "User already exists." });
        }
      } else {
        reject({ code: 0, msg: err });
      }
    });
  });

exports.update = user =>
  new Promise((resolve, reject) => {
    console.log(user);

    User.findOneAndUpdate(
      { id: user.id },
      {
        $set: {
          // email: user.email,
          //password: user.password,
          workTime: user.workTime
        }
      },
      { new: true },
      (err, newUser) => {
        if (!err) {
          if (!newUser) {
            reject("User does not updated.");
          }
          resolve(newUser);
        } else {
          reject(err);
        }
      }
    );
  });
