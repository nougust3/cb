const Customer = require("../models/customer");
const TaskController = require("./task.controller");
const uuidV1 = require("uuid/v1");

function generateId() {
  return uuidV1();
}

exports.get = customerId =>
  new Promise((resolve, reject) => {
    Customer.findOne({ id: customerId }, (err, customer) => {
      if (!err) {
        if (customer) {
          resolve(customer);
        } else {
          reject("Customer not found.");
        }
      } else {
        reject(err);
      }
    });
  });

exports.getAll = owner =>
  new Promise((resolve, reject) =>
    TaskController.getAll(owner)
      .then(tasks => tasks.map(task => task.customer))
      .then(customers => {
        Customer.find({ id: { $in: customers } }, (err, customers) => {
          if (!err) {
            if (customers) {
              resolve(customers);
            } else {
              reject("Not found");
            }
          } else {
            reject(err);
          }
        });
      })
      .catch(err => reject(err))
  );

exports.getCustomers = customers =>
  new Promise((resolve, reject) => {
    Customer.find({ $or: [...customers] }, (err, customers) => {
      if (!err) {
        if (customers) {
          resolve(customers);
        } else {
          reject("Not found");
        }
      } else {
        reject(err);
      }
    });
  });

exports.find = customerData =>
  new Promise((resolve, reject) => {
    if (customerData.id) {
      resolve(customerData.id);
    } else {
      Customer.findOne(
        {
          $or: [{ email: customerData.email }, { phone: customerData.phone }]
        },
        (err, customer) => {
          if (!err) {
            if (customer) {
              console.log(customer);
              resolve(customer.id);
            } else {
              resolve(0);
            }
          } else {
            reject(err);
          }
        }
      );
    }
  });

exports.create = customer =>
  new Promise((resolve, reject) => {
    const newCustomer = new Customer();

    newCustomer.id = generateId();
    newCustomer.email = customer.email;
    newCustomer.phone = customer.phone;
    newCustomer.name = customer.name;
    newCustomer.info = customer.info;
    newCustomer.save(err => {
      if (err) {
        reject(err);
      }
      resolve(newCustomer);
    });
  });

exports.update = customer =>
  new Promise((resolve, reject) => {
    Customer.findOneAndUpdate(
      { id: customer.id },
      {
        $set: {
          name: customer.name,
          phone: customer.phone,
          email: customer.email,
          info: customer.info
        }
      },
      { new: true },
      (err, newCustomer) => {
        if (!err) {
          if (!newCustomer) {
            reject("Customer does not updated.");
          }
          resolve(newCustomer);
        } else {
          reject(err);
        }
      }
    );
  });
