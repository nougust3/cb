const Service = require("../models/service");
const uuidV1 = require("uuid/v1");

function generateId() {
  return uuidV1();
}

exports.get = serviceId =>
  new Promise((resolve, reject) => {
    Service.findOne({ id: serviceId }, (err, service) => {
      if (!err) {
        if (!service) {
          reject("Service not found.");
        } else {
          resolve(service);
        }
      } else {
        reject(err);
      }
    });
  });

exports.getAll = owner =>
  new Promise((resolve, reject) => {
    Service.find({ owner }, (err, services) => {
      if (!err) {
        if (!services) {
          reject("Services not found.");
        } else {
          resolve(services);
        }
      } else {
        reject(err);
      }
    });
  });

exports.create = (owner, service) =>
  new Promise((resolve, reject) => {
    Service.findOne(
      { owner: service.owner, name: service.name },
      (err, service1) => {
        if (!err) {
          if (!service1) {
            const newService = new Service();

            newService.id = generateId();
            newService.owner = owner;
            newService.name = service.name;
            newService.description = service.description;
            newService.price = service.price;
            newService.save(err1 => {
              if (err1) {
                reject(err1);
              }
              resolve(newService);
            });
          } else {
            reject("Service already exists.");
          }
        } else {
          reject(err);
        }
      }
    );
  });

exports.remove = serviceId =>
  new Promise((resolve, reject) => {
    Service.findOneAndRemove({ id: serviceId }, err => {
      if (!err) {
        resolve();
      } else {
        reject(err);
      }
    });
  });

exports.update = service =>
  new Promise((resolve, reject) => {
    Service.findOneAndUpdate(
      { id: service.id },
      {
        $set: {
          name: service.name,
          price: service.price,
          description: service.description
        }
      },
      { new: true },
      (err, newService) => {
        if (!err) {
          if (!newService) {
            reject("Service does not updated.");
          }
          resolve(newService);
        } else {
          reject();
        }
      }
    );
  });
