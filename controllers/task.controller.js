const Task = require("../models/task");
const uuidV1 = require("uuid/v1");

const CustomerController = require("./../controllers/customer.controller");

function generateId() {
  return uuidV1();
}

exports.create = taskData =>
  new Promise((resolve, reject) => {
    CustomerController.find({
      phone: taskData.customer.phone,
      email: taskData.customer.email,
      id: taskData.customerId
    })
      .then(customerId => {
        if (customerId != 0) {
          return CustomerController.get(customerId);
        }
        return CustomerController.create({
          name: taskData.customer.name,
          email: taskData.customer.email,
          phone: taskData.customer.phone,
          info: ""
        });
      })
      .then(customer =>
        exports.createTask(customer.id, {
          service: taskData.service,
          owner: taskData.owner,
          time: taskData.time
        })
      )
      .then(newTask => resolve(newTask.id))
      .catch(err => reject(err));
  });

exports.remove = taskId =>
  new Promise((resolve, reject) => {
    Task.findOneAndRemove({ id: taskId }, err => {
      if (!err) {
        resolve();
      } else {
        reject(err);
      }
    });
  });

exports.update = task =>
  new Promise((resolve, reject) => {
    Task.findOneAndUpdate(
      { id: task.id },
      {
        $set: {
          time: task.time,
          comment: task.comment,
          customer: task.customer,
          service: task.service
        }
      },
      { new: true },
      (err, newTask) => {
        if (!err) {
          if (!newTask) {
            reject("Task does not updated.");
          }
          resolve(newTask);
        } else {
          reject(err);
        }
      }
    );
  });

exports.createTask = (customerId, task) =>
  new Promise((resolve, reject) => {
    const newTask = new Task();

    newTask.id = generateId();
    newTask.customer = customerId;
    newTask.service = task.service;
    newTask.owner = task.owner;
    newTask.time = task.time;
    newTask.save(err => {
      if (!err) {
        resolve(newTask);
      } else {
        reject(err);
      }
    });
  });

exports.getAll = owner =>
  new Promise((resolve, reject) => {
    Task.find({ owner }, (err, tasks) => {
      if (!err) {
        if (!tasks) {
          reject("Tasks not found.");
        } else {
          resolve(tasks);
        }
      } else {
        reject(err);
      }
    });
  });

exports.getByCustomer = (owner, customer) =>
  new Promise((resolve, reject) => {
    Task.find({ owner, customer }, (err, tasks) => {
      if (!err) {
        if (!tasks) {
          reject("Tasks not found.");
        } else {
          resolve(tasks);
        }
      } else {
        reject(err);
      }
    });
  });
