import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { UserService } from "./services/user.service";
import { ServicesService } from "./services/services.service";
import { AuthService } from "./services/auth.service";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Router, RouteReuseStrategy } from "@angular/router";
import { APP_BASE_HREF } from "@angular/common";

describe("AppComponent | Основной компонент приложения", () => {
  let mockRouter = {
    navigate: jasmine.createSpy("navigate")
  };
  let mockService;
  let fixture;

  let isSigned = false;

  class MockService {
    isSigned() {
      return isSigned;
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        RouterModule.forRoot([], { enableTracing: true }),
        HttpClientModule
      ],
      providers: [
        UserService,
        ServicesService,
        AuthService,
        { provide: APP_BASE_HREF, useValue: "/" },
        { provide: Router, useValue: mockRouter },
        { provide: UserService, useClass: MockService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
  });

  it("создается", () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(
    "загружает страницу авторизации",
    async(() => {
      expect(mockRouter.navigate).toHaveBeenCalledWith(["/auth"]);
      isSigned = true;
    })
  );

  it(
    "загружает страницу профиля",
    async(() => {
      expect(mockRouter.navigate).toHaveBeenCalledWith(["/profile"]);
    })
  );
});
