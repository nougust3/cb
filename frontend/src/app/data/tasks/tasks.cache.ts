import { Task } from "../../models/task";
import * as Rx from "rxjs";

export class TasksCache {
  tasks: Task[] = [];
  tasksSubject: Rx.Subject<Task[]>;

  constructor() {
    this.tasksSubject = new Rx.Subject();
  }

  onDataChange(): Rx.Subject<Task[]> {
    return this.tasksSubject;
  }

  isEmpty(): boolean {
    return this.tasks.length === 0;
  }

  getAll(): Task[] {
    return this.tasks;
  }

  get(id: string): Task {
    return this.tasks.find(s => s.id === id);
  }

  update(service: Task): void {
    this.tasks = this.tasks.map(s => (s.id === service.id ? service : s));
    this.tasksSubject.next(this.tasks);
  }

  store(task: Task): void {
    this.tasks.push(task);
    this.tasksSubject.next(this.tasks);
  }

  remove(id: string): void {
    this.tasks = this.tasks.filter(s => s.id !== id);
    this.tasksSubject.next(this.tasks);
  }
}
