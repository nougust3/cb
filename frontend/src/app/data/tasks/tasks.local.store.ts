import { TasksDataStore } from "./tasks.store";
import { TasksCache } from "./tasks.cache";
import { Task } from "../../models/task";
import * as Rx from "rxjs";

export class TasksLocalDataStore implements TasksDataStore {
  onDataChange(): Rx.Subject<Task[]> {
    return this.tasksCahce.onDataChange();
  }
  constructor(private tasksCahce: TasksCache) {}

  getAll(): Task[] {
    return this.tasksCahce.getAll();
  }

  get(id: string): Task {
    return this.tasksCahce.get(id);
  }

  update(task: Task) {
    this.tasksCahce.update(task);
  }

  store(task: Task): void {
    this.tasksCahce.store(task);
  }

  remove(id: string): void {
    this.tasksCahce.remove(id);
  }
}
