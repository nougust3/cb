import { TasksDataStore } from "./tasks.store";
import { Task } from "../../models/task";
import * as Rx from "rxjs";
import { HttpClient } from "@angular/common/http";
import { endpoints } from "../../../environments/environment";
import { TasksCache } from "./tasks.cache";

export class TasksRemoteDataStore implements TasksDataStore {
  email;

  constructor(private tasksCache: TasksCache, private http: HttpClient) {
    this.email = localStorage.getItem("email");
  }

  getAll(): Task[] {
    this.http
      .post(endpoints.task.all, { email: this.email })
      .subscribe(response => {
        if (response["success"]) {
          let tasks = response["tasks"];

          for (let task of tasks) {
            this.tasksCache.store({
              id: task.id,
              service: task.service,
              customer: task.customer,
              time: task.time,
              owner: "",
              comment: ""
            });
          }

          this.tasksCache.tasksSubject.next(this.tasksCache.getAll());
        }
      });

    return this.tasksCache.getAll();
  }

  get(id: string): Task {
    return this.tasksCache.get(id);
  }

  update(task: Task): void {
    this.http.post(endpoints.task.update, task).subscribe(response => {
      this.tasksCache.update(task);
    });
  }

  store(task: Task): void {
    task.owner = this.email;
    this.http.post(endpoints.task.store, task).subscribe(response => {
      if (!response["success"]) {
        console.log(response["msg"]);
      }
    });
  }

  remove(id: string): void {
    this.http.post(endpoints.task.remove, { id }).subscribe(response => {
      if (!response["success"]) {
        console.log(response["msg"]);
      }
    });
  }
}
