import { TasksRemoteDataStore } from "./tasks.remote.store";
import { TasksLocalDataStore } from "./tasks.local.store";
import { HttpClient } from "@angular/common/http";
import { TasksCache } from "./tasks.cache";

export class TasksDataStoreFactory {
  constructor(private tasksCache: TasksCache, private http: HttpClient) {}

  createLocal(): TasksLocalDataStore {
    return new TasksLocalDataStore(this.tasksCache);
  }
  createRemote(): TasksRemoteDataStore {
    return new TasksRemoteDataStore(this.tasksCache, this.http);
  }
}
