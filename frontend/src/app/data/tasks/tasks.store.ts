import { Task } from "../../models/task";

export interface TasksDataStore {
  getAll(): Task[];
  get(id: string): void;
  update(task: Task): void;
  store(task: Task): void;
  remove(id: string): void;
}
