import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TasksDataStoreFactory } from "./tasks.store.factory";
import { TasksRemoteDataStore } from "./tasks.remote.store";
import { TasksLocalDataStore } from "./tasks.local.store";
import { Task } from "../../models/task";
import * as Rx from "rxjs";

export class TasksRepository {
  localDataStore: TasksLocalDataStore;
  remoteDataStore: TasksRemoteDataStore;

  constructor(tasksDataStoreFactory: TasksDataStoreFactory) {
    this.localDataStore = tasksDataStoreFactory.createLocal();
    this.remoteDataStore = tasksDataStoreFactory.createRemote();

    this.remoteDataStore.getAll();
  }

  onDataChange(): Rx.Subject<Task[]> {
    return this.localDataStore.onDataChange();
  }

  getAll(): Task[] {
    return this.localDataStore.getAll();
  }

  get(id: string): Task {
    return this.localDataStore.get(id);
  }

  update(task: Task): void {
    this.remoteDataStore.update(task);
    return this.localDataStore.update(task);
  }

  remove(id: string): void {
    this.remoteDataStore.remove(id);
    this.localDataStore.remove(id);
  }

  store(task: Task): void {
    this.remoteDataStore.store(task);
    this.localDataStore.store(task);
  }
}
