import { Service } from "./../../models/service";

export interface ServiceDataStore {
  getAll(): Service[];
  get(id: string): void;
  update(service: Service): void;
  store(service: Service): void;
  remove(id: string): void;
}
