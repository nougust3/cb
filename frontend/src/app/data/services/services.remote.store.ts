import { ServiceDataStore } from "./services.store";
import { Service } from "../../models/service";
import * as Rx from "rxjs";
import { HttpClient } from "@angular/common/http";
import { endpoints } from "../../../environments/environment";
import { ServicesCache } from "./services.cache";

export class ServicesRemoteDataStore implements ServiceDataStore {
  email;

  constructor(private servicesCache: ServicesCache, private http: HttpClient) {
    this.email = localStorage.getItem("email");
  }

  getAll(): Service[] {
    this.http
      .post(endpoints.service.all, { email: this.email })
      .subscribe(response => {
        if (response["success"]) {
          let services = response["services"];

          for (let service of services) {
            this.servicesCache.store({
              id: service.id,
              name: service.name,
              description: service.description,
              price: service.price
            });
          }

          this.servicesCache.servicesSubject.next(this.servicesCache.getAll());
        }
      });

    return this.servicesCache.getAll();
  }

  get(id: string): Service {
    return this.servicesCache.get(id);
  }

  update(service: Service): void {
    this.http.post(endpoints.service.update, service).subscribe(response => {
      this.servicesCache.update(service);
    });
  }

  store(service: Service): void {
    this.http
      .post(endpoints.service.store, { email: this.email, service })
      .subscribe(response => {
        console.log({ email: this.email, service });
        if (!response["success"]) {
          console.log(response["msg"]);
        }
      });
  }

  remove(id: string): void {
    this.http
      .post(endpoints.service.all, { email: this.email })
      .subscribe(response => {
        if (!response["success"]) {
          console.log(response["msg"]);
        }
      });
  }
}
