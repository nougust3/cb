import { ServicesRemoteDataStore } from "./services.remote.store";
import { ServicesLocalDataStore } from "./services.local.store";
import { HttpClient } from "@angular/common/http";
import { ServicesCache } from "./services.cache";

export class ServicesDataStoreFactory {
  constructor(private servicesCache: ServicesCache, private http: HttpClient) {}

  createLocal(): ServicesLocalDataStore {
    return new ServicesLocalDataStore(this.servicesCache);
  }
  createRemote(): ServicesRemoteDataStore {
    return new ServicesRemoteDataStore(this.servicesCache, this.http);
  }
}
