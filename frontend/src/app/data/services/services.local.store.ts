import { ServiceDataStore } from "./services.store";
import { ServicesCache } from "./services.cache";
import { Service } from "../../models/service";
import * as Rx from "rxjs";

export class ServicesLocalDataStore implements ServiceDataStore {
  onDataChange(): Rx.Subject<Service[]> {
    return this.servicesCahce.onDataChange();
  }
  constructor(private servicesCahce: ServicesCache) {}

  getAll(): Service[] {
    return this.servicesCahce.getAll();
  }

  get(id: string): Service {
    return this.servicesCahce.get(id);
  }

  update(service: Service) {
    this.servicesCahce.update(service);
  }

  store(service: Service): void {
    this.servicesCahce.store(service);
  }

  remove(id: string): void {
    this.servicesCahce.remove(id);
  }
}
