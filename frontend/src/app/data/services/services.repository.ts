import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ServicesDataStoreFactory } from "./services.store.factory";
import { ServicesRemoteDataStore } from "./services.remote.store";
import { ServicesLocalDataStore } from "./services.local.store";
import { Service } from "../../models/service";
import * as Rx from "rxjs";

export class ServicesRepository {
  localDataStore: ServicesLocalDataStore;
  remoteDataStore: ServicesRemoteDataStore;

  constructor(sericesDataStoreFactory: ServicesDataStoreFactory) {
    this.localDataStore = sericesDataStoreFactory.createLocal();
    this.remoteDataStore = sericesDataStoreFactory.createRemote();

    this.remoteDataStore.getAll();
  }

  onDataChange(): Rx.Subject<Service[]> {
    return this.localDataStore.onDataChange();
  }

  getAll(): Service[] {
    return this.localDataStore.getAll();
  }

  get(id: string): Service {
    return this.localDataStore.get(id);
  }

  update(service: Service): void {
    this.remoteDataStore.update(service);
    return this.localDataStore.update(service);
  }

  remove(id: string): void {
    this.remoteDataStore.remove(id);
    this.localDataStore.remove(id);
  }

  store(service: Service): void {
    this.remoteDataStore.store(service);
    this.localDataStore.store(service);
  }
}
