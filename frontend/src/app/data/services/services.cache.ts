import { Service } from "../../models/service";
import * as Rx from "rxjs";

export class ServicesCache {
  services: Service[] = [];
  servicesSubject: Rx.Subject<Service[]>;

  constructor() {
    this.servicesSubject = new Rx.Subject();
  }

  onDataChange(): Rx.Subject<Service[]> {
    return this.servicesSubject;
  }

  isEmpty(): boolean {
    return this.services.length === 0;
  }

  getAll(): Service[] {
    return this.services;
  }

  get(id: string): Service {
    return this.services.find(service => service.id === id);
  }

  update(service: Service): void {
    this.services = this.services.map(
      service1 => (service.id === service1.id ? service1 : service)
    );
    this.servicesSubject.next(this.services);
  }

  store(service: Service): void {
    this.services.push(service);
    this.servicesSubject.next(this.services);
  }

  remove(id: string): void {
    this.services = this.services.filter(service => service.id !== id);
    this.servicesSubject.next(this.services);
  }
}
