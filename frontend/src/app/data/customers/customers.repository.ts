import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CustomersDataStoreFactory } from "./customers.store.factory";
import { CustomersRemoteDataStore } from "./customers.remote.store";
import { CustomersLocalDataStore } from "./customers.local.store";
import { Customer } from "../../models/customer";
import * as Rx from "rxjs";

export class CustomersRepository {
  localDataStore: CustomersLocalDataStore;
  remoteDataStore: CustomersRemoteDataStore;

  constructor(sericesDataStoreFactory: CustomersDataStoreFactory) {
    this.localDataStore = sericesDataStoreFactory.createLocal();
    this.remoteDataStore = sericesDataStoreFactory.createRemote();

    this.remoteDataStore.getAll();
  }

  onDataChange(): Rx.Subject<Customer[]> {
    return this.localDataStore.onDataChange();
  }

  getAll(): Customer[] {
    return this.localDataStore.getAll();
  }

  get(id: string): Customer {
    return this.localDataStore.get(id);
  }

  update(customer: Customer): void {
    this.remoteDataStore.update(customer);
    return this.localDataStore.update(customer);
  }

  remove(id: string): void {
    this.remoteDataStore.remove(id);
    this.localDataStore.remove(id);
  }

  store(customer: Customer): void {
    this.remoteDataStore.store(customer);
    this.localDataStore.store(customer);
  }
}
