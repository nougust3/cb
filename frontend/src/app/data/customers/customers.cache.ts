import { Customer } from "../../models/customer";
import * as Rx from "rxjs";

export class CustomersCache {
  customers: Customer[] = [];
  customersSubject: Rx.Subject<Customer[]>;

  constructor() {
    this.customersSubject = new Rx.Subject();
  }

  onDataChange(): Rx.Subject<Customer[]> {
    return this.customersSubject;
  }

  isEmpty(): boolean {
    return this.customers.length === 0;
  }

  getAll(): Customer[] {
    return this.customers;
  }

  get(id: string): Customer {
    return this.customers.find(s => s.id === id);
  }

  update(customer: Customer): void {
    this.customers = this.customers.map(
      s => (s.id === customer.id ? customer : s)
    );
    this.customersSubject.next(this.customers);
  }

  store(customer: Customer): void {
    this.customers.push(customer);
    this.customersSubject.next(this.customers);
  }

  remove(id: string): void {
    this.customers = this.customers.filter(s => s.id !== id);
    this.customersSubject.next(this.customers);
  }
}
