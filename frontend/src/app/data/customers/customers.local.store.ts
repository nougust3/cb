import { CustomerDataStore } from "./customers.store";
import { CustomersCache } from "./customers.cache";
import { Customer } from "../../models/customer";
import * as Rx from "rxjs";

export class CustomersLocalDataStore implements CustomerDataStore {
  onDataChange(): Rx.Subject<Customer[]> {
    return this.customersCahce.onDataChange();
  }
  constructor(private customersCahce: CustomersCache) {}

  getAll(): Customer[] {
    return this.customersCahce.getAll();
  }

  get(id: string): Customer {
    return this.customersCahce.get(id);
  }

  update(customer: Customer) {
    this.customersCahce.update(customer);
  }

  store(customer: Customer): void {
    this.customersCahce.store(customer);
  }

  remove(id: string): void {
    this.customersCahce.remove(id);
  }
}
