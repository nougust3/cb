import { CustomersRemoteDataStore } from "./customers.remote.store";
import { CustomersLocalDataStore } from "./customers.local.store";
import { HttpClient } from "@angular/common/http";
import { CustomersCache } from "./customers.cache";

export class CustomersDataStoreFactory {
  constructor(
    private customersCache: CustomersCache,
    private http: HttpClient
  ) {}

  createLocal(): CustomersLocalDataStore {
    return new CustomersLocalDataStore(this.customersCache);
  }
  createRemote(): CustomersRemoteDataStore {
    return new CustomersRemoteDataStore(this.customersCache, this.http);
  }
}
