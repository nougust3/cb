import { CustomerDataStore } from "./customers.store";
import { Customer } from "../../models/customer";
import * as Rx from "rxjs";
import { HttpClient } from "@angular/common/http";
import { endpoints } from "../../../environments/environment";
import { CustomersCache } from "./customers.cache";

export class CustomersRemoteDataStore implements CustomerDataStore {
  email;

  constructor(
    private customersCache: CustomersCache,
    private http: HttpClient
  ) {
    this.email = localStorage.getItem("email");
  }

  getAll(): Customer[] {
    this.http
      .post(endpoints.customer.all, { email: this.email })
      .subscribe(response => {
        if (response["success"]) {
          let customers = response["customers"];

          for (let customer of customers) {
            this.customersCache.store({
              id: customer.id,
              name: customer.name,
              phone: customer.phone,
              email: customer.email,
              info: customer.info
            });
          }

          this.customersCache.customersSubject.next();
        }
      });
    return this.customersCache.getAll();
  }

  get(id: string): Customer {
    return this.customersCache.get(id);
  }

  update(customer: Customer): void {
    this.http.post(endpoints.customer.update, customer).subscribe(response => {
      this.customersCache.update(customer);
    });
  }

  store(customer: Customer): void {
    this.http
      .post(endpoints.customer.store, { customer })
      .subscribe(response => {
        if (!response["success"]) {
          console.log(response["msg"]);
        }
      });
  }

  remove(id: string): void {
    this.http
      .post(endpoints.customer.all, { email: this.email })
      .subscribe(response => {
        if (!response["success"]) {
          console.log(response["msg"]);
        }
      });
  }
}
