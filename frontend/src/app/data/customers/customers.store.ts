import { Customer } from "./../../models/customer";

export interface CustomerDataStore {
  getAll(): Customer[];
  get(id: string): void;
  update(customer: Customer): void;
  store(customer: Customer): void;
  remove(id: string): void;
}
