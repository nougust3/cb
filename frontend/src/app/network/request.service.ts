import { Injectable } from "@angular/core";
import { HttpClient, HttpHandler, HttpRequest } from "@angular/common/http";
import * as Rx from "rxjs";

@Injectable()
export class RequestService {
  isComplite = true;
  requests = [];
  errorSubject: Rx.Subject<any>;
  requestsSubject: Rx.Subject<any>;

  constructor(private http: HttpHandler) {
    this.errorSubject = new Rx.Subject();
    this.requestsSubject = new Rx.Subject();
  }

  onError() {
    return this.errorSubject;
  }

  push(url: string, data: any) {
    this.requests.push(new HttpRequest("post", url, data));

    if (this.isComplite) {
      this.start();
      this.requestsSubject.next();
    }
  }

  start() {
    this.isComplite = false;
    this.requestsSubject.subscribe(() => {
      const request = this.requests.shift();

      if (request) {
        this.http.handle(request).subscribe(res => {
          if (res["status"]) {
            if (res["status"] === 200) {
              this.requestsSubject.next();
            } else {
              this.errorSubject.next();
            }
          }
        });
      } else {
        this.isComplite = true;
      }
    });
  }
}
