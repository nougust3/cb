import { Component } from "@angular/core";

@Component({
  selector: "empty",
  template: "<p>{{message}}</p>",
  styles: [
    `p {
      display: block;
      text-align: center;
      color: #303334;
      padding: 20px 0 40px 0;
    }`
  ]
})
export class EmptyComponent {
  message: string = "Список пуст";

  setMessage(message) {
    this.message = message;
  }
}
