import {
  Component,
  OnInit,
  Inject,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";

import * as moment from "moment";
import { TimePickerComponent } from "../../../timePicker/timePicker.component";
import { BaseComponent } from "../../../base.component";
import { format } from "../../../../../environments/environment";

@Component({
  selector: "app-appoint-dialog",
  templateUrl: "appointDialog.component.html",
  styleUrls: ["./appointDialog.component.scss"]
})
export class AppointDialogComponent extends BaseComponent {
  @ViewChild("timePicker") timePicker: TimePickerComponent;

  currentDate;
  service;
  customer;
  workHours = [];
  @Output() comment: "";

  @Output() time = { hours: 0, minutes: 0 };
  @Output() res = "";

  constructor(
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AppointDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(snackBar);
    this.currentDate = moment().format(format.date);

    for (let hour = 0; hour < 24; hour++) {
      this.workHours.push(hour);
    }

    if (data.customers.length === 1) {
      this.customer = data.customers[0].id;
    }
  }

  ngOnInit(): void {
    this.timePicker.setHoursRange(
      this.data.workTime.start,
      this.data.workTime.end
    );
  }

  onNextDate() {
    this.currentDate = moment(this.currentDate, format.date)
      .add(1, "days")
      .format(format.date);
  }

  onPrevDate() {
    this.currentDate = moment(this.currentDate, format.date)
      .subtract(1, "days")
      .format(format.date);
  }

  onSelectHour(hours) {
    this.time.hours = hours;
  }

  onSelectMinutes(minutes) {
    this.time.minutes = minutes;
  }

  onSelectCustomer(customer) {
    console.log(customer);
    this.customer = customer;
  }

  onSelectService(service) {
    this.service = service;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    if (!this.customer) {
      this.showMsg("Укажите клиента");
    } else if (!this.service) {
      this.showMsg("Укажите услугу");
    } else if (!this.timePicker.getTime()) {
      this.showMsg("Укажите время");
    } else {
      this.dialogRef.close({
        customer: this.customer,
        service: this.service,
        time: this.timePicker.getTime()
      });
    }
  }
}
