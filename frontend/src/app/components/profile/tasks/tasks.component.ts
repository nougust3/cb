import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";
import { TaskService } from "../../../services/task.service";
import { ServicesService } from "../../../services/services.service";
import { CustomerService } from "../../../services/customer.service";
import { AppointDialogComponent } from "./appointDialog/appointDialog.component";
import { FacadeService } from "../../../services/facade.service";

import * as moment from "moment";
import { ShiftDialogComponent } from "./shiftDialog/shiftDialog.component";
import { TimePickerComponent } from "../../timePicker/timePicker.component";
import { BaseComponent } from "../../base.component";

@Component({
  selector: "app-tasks",
  templateUrl: "./tasks.component.html",
  styleUrls: ["./tasks.component.scss"]
})
export class TasksComponent implements OnInit {
  tasks = [];
  appointDialog;
  shiftDialog;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private service: FacadeService
  ) {}

  ngOnInit(): void {
    this.service.tasks
      .onDataChange()
      .subscribe(() => this.populate(this.service.tasks.getAll()));
    this.service.services
      .onDataChange()
      .subscribe(() => this.populate(this.service.tasks.getAll()));
    this.service.customers.onDataChange().subscribe(() => {
      this.populate(this.service.tasks.getAll());
    });
  }

  onCreateTask(): void {
    this.appointDialog = this.dialog.open(AppointDialogComponent, {
      data: {
        services: this.service.services.getAll(),
        customers: this.service.customers.getAll(),
        workTime: this.service.user.getWorkTime()
      }
    });
    this.appointDialog.afterClosed().subscribe(taskData => {
      if (taskData) {
        this.service.tasks.store(taskData);
      }
    });
  }

  onCancelTask(task): void {
    this.service.tasks.remove(task);
  }

  onShiftTask(task): void {
    this.shiftDialog = this.dialog.open(ShiftDialogComponent, {
      data: {
        workTime: this.service.user.getWorkTime()
      }
    });
    this.shiftDialog.afterClosed().subscribe(time => {
      if (time) {
        task.time = time;
        this.service.tasks.update(task);
      }
    });
  }

  getCustomerName(id) {
    let customer = this.service.customers.get(id);

    return customer ? customer.name : "Неизвестный клиент";
  }

  getServiceName(id) {
    let service = this.service.services.get(id);

    return service ? service.name : "Неизвестная услуга";
  }

  populate(tasks) {
    this.tasks.length = 0;
    for (let task of tasks) {
      task.customerName = this.getCustomerName(task.customer);
      task.serviceName = this.getServiceName(task.service);
      task.timeView = moment(task.time, "x").format("hh:mm");
      this.tasks.push(task);
    }
  }
}
