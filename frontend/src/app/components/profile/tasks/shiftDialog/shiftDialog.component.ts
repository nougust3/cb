import {
  Component,
  OnInit,
  Inject,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";

import * as moment from "moment";
import { TimePickerComponent } from "../../../timePicker/timePicker.component";
import { BaseComponent } from "../../../base.component";

@Component({
  selector: "app-shift-dialog",
  templateUrl: "shiftDialog.component.html",
  styleUrls: ["./shiftDialog.component.scss"]
})
export class ShiftDialogComponent extends BaseComponent implements OnInit {
  @ViewChild("timePicker") timePicker: TimePickerComponent;

  constructor(
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ShiftDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(snackBar);
  }

  ngOnInit(): void {
    this.timePicker.setHoursRange(
      this.data.workTime.start,
      this.data.workTime.end
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    let time = this.timePicker.getTime();

    if (time) {
      this.dialogRef.close(this.timePicker.getTime());
    } else {
      this.showMsg("Укажите время");
    }
  }
}
