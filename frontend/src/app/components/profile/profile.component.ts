import { Component, OnInit, Input } from "@angular/core";
import { FacadeService } from "../../services/facade.service";
import * as Rx from "rxjs";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  @Input() email = "";

  @Input() workTime;

  startTime = new Rx.Subject<string>();
  endTime = new Rx.Subject<string>();

  constructor(private service: FacadeService) {
    this.email = service.user.getEmail();
    this.workTime = service.user.getWorkTime();
  }

  ngOnInit(): void {
    this.startTime.subscribe(request => {
      this.service.user.setWorkTime({
        start: this.workTime.start,
        end: this.workTime.end
      });
    });
    this.endTime.subscribe(request => {
      this.service.user.setWorkTime({
        start: this.workTime.start,
        end: this.workTime.end
      });
    });
    this.service.user.getUser().subscribe(user => {
      this.email = user["email"];
      this.workTime = this.service.user.getWorkTime();
    });
  }

  signOut(): void {
    this.service.user.signOut();
  }
}
