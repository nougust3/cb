import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ProfileComponent } from "./profile.component";
import {
  MatToolbarModule,
  MatTabsModule,
  MatFormField,
  MatInputModule
} from "@angular/material";
import { UserService } from "../../services/user.service";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { APP_BASE_HREF } from "@angular/common";
import { TaskService } from "../../services/task.service";
import { FormsModule } from "@angular/forms";
import { FacadeService } from "../../services/facade.service";
import { RequestService } from "../../network/request.service";
import { RSA_X931_PADDING } from "constants";
import * as Rx from "rxjs";

describe("ProfileComponent", () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  const userServiceStub = {
    getEmail() {
      return "email";
    },
    getWorkTime() {
      return { start: 10, end: 20 };
    },
    getUser() {
      return Rx.Observable.from([
        { email: "email", workTime: { start: 10, end: 20 } }
      ]);
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent],
      imports: [
        RouterModule.forRoot([], { enableTracing: true }),
        HttpClientModule,
        MatToolbarModule,
        MatTabsModule,
        BrowserAnimationsModule,
        FormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        FacadeService,
        RequestService,
        { provide: APP_BASE_HREF, useValue: "/" },
        { provide: UserService, useValue: userServiceStub }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
  });

  it("создается", () => {
    expect(component).toBeTruthy();
  });

  it("отображает имя пользователя", () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".email").textContent).toEqual("email");
  });
});
