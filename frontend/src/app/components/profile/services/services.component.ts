import { Component, OnInit, ViewChild } from "@angular/core";
import { ServiceDialogComponent } from "./dialog/dialog.component";
import { EmptyComponent } from "./../empty.component";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";
import { ServicesService } from "../../../services/services.service";
import { FacadeService } from "../../../services/facade.service";

@Component({
  selector: "app-services",
  templateUrl: "./services.component.html",
  styleUrls: ["./services.component.scss"]
})
export class ServicesComponent implements OnInit {
  services = [];
  createDialog;
  editDialog;

  constructor(
    private dialog: MatDialog,
    private service: FacadeService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.service.services
      .onDataChange()
      .subscribe(services => (this.services = services));
  }

  onCreateService(): void {
    this.createDialog = this.dialog.open(ServiceDialogComponent);
    this.createDialog.afterClosed().subscribe(serviceData => {
      if (serviceData) {
        this.service.services.store(serviceData);
      }
    });
  }

  onUpdateService(service): void {
    this.editDialog = this.dialog.open(ServiceDialogComponent, {
      data: {
        isNew: false,
        name: service.name,
        description: service.description,
        price: service.price
      }
    });
    this.editDialog.afterClosed().subscribe(serviceData => {
      if (serviceData) {
        serviceData.id = service.id;
        this.service.services.update(serviceData);
      }
    });
  }
  showMessage(message: string) {
    this.snackBar.open(message, "", { duration: 3000 });
  }

  onRemoveService(service) {
    this.service.services.remove(service.id);
  }
}
