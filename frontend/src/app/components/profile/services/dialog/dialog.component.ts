import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-service-dialog",
  templateUrl: "dialog.component.html",
  styleUrls: ["./dialog.component.scss"]
})
export class ServiceDialogComponent {
  isNew = true;
  name: "";
  price: "";
  description: "";

  constructor(
    public dialogRef: MatDialogRef<ServiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data) {
      this.isNew = data.isNew;
      this.name = data.name;
      this.price = data.price;
      this.description = data.description;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.dialogRef.close({
      name: this.name,
      price: this.price,
      description: this.description
    });
  }
}
