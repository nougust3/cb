import { Component, OnInit, Inject } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";

@Component({
  selector: "app-allappoints-dialog",
  templateUrl: "./allAppointsDialog.component.html",
  styleUrls: ["./allAppointsDialog.component.scss"]
})
export class AllAppointsDialogComponent {
  tasks;

  constructor(
    public dialogRef: MatDialogRef<AllAppointsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data) {
      this.tasks = data.tasks;
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
