import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Customer } from "../../../../models/customer";

@Component({
  selector: "edit-customer-dialog",
  templateUrl: "editDialog.component.html"
})
export class EditCustomerDialogComponent {
  isNew = true;

  name = "";
  phone = "";
  email = "";
  info = "";

  services;

  constructor(
    public dialogRef: MatDialogRef<EditCustomerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.customer) {
      this.isNew = false;
      this.name = data.customer.name;
      this.phone = data.customer.phone;
      this.email = data.customer.email;
      this.info = data.customer.info;
    }
    if(data.services) {
      this.services = data.services;
    }
  }

  onDone(): void {
    this.dialogRef.close({
      isNew: this.isNew,
      customer: {
        id: this.isNew ? "" : this.data.customer.id,
        name: this.name,
        phone: this.phone,
        email: this.email,
        info: this.info
      }
    });
  }
}
