import { Component, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";
import { ServicesService } from "../../../services/services.service";
import { FacadeService } from "../../../services/facade.service";
import { AllAppointsDialogComponent } from "./allAppointsDialog/allAppointsDialog.component";
import { AppointDialogComponent } from "../tasks/appointDialog/appointDialog.component";
import { EditCustomerDialogComponent } from "./editDialog/editDialog.component";
import { Subject } from "rxjs";

@Component({
  selector: "app-customers",
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.scss"]
})
export class CustomersComponent implements OnInit {
  customers = [];
  editDialog;
  appointDialog;
  allAppointsDialog;

  isSearch = false;
  searchRequest = new Subject<string>();

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private service: FacadeService
  ) {}

  ngOnInit(): void {
    this.service.customers.onDataChange().subscribe(customers => {
      this.populate(this.service.customers.getAll());
    });
    this.searchRequest.subscribe(request => {
      this.isSearch = !!request.length;
      this.populate(this.service.customers.search(request));
    });
  }

  onEdit(customer) {
    this.editDialog = this.dialog.open(EditCustomerDialogComponent, {
      data: {
        customer
      }
    });
    this.editDialog.afterClosed().subscribe(result => {
      if (result) {
        if (result.isNew) {
          this.service.customers.store(result.customer);
        } else {
          this.service.customers.update(result.customer);
        }
      }
    });
  }

  onAppoint(customer) {
    this.appointDialog = this.dialog.open(AppointDialogComponent, {
      data: {
        customers: [customer],
        services: this.service.services.getAll(),
        workTime: this.service.user.getWorkTime()
      }
    });
    this.appointDialog.afterClosed().subscribe(taskData => {
      if (taskData) {
        this.service.tasks.store(taskData);
      }
    });
  }

  showAllAppoints(customer) {
    this.allAppointsDialog = this.dialog.open(AllAppointsDialogComponent, {
      data: {
        tasks: this.getCustomerTasks(customer.id)
      }
    });
  }

  private getCustomerTasks(id) {
    let tasks = this.service.tasks.getByCustomer(id);
    let res = [];

    tasks.forEach(task => {
      const service = this.service.services.get(task.service);
      res.push({
        serviceName: service.name,
        servicePrice: service.price
      });
    });

    return res;
  }

  populate(customers) {
    this.customers.length = 0;
    for (let customer of customers) {
      this.customers.push(customer);
    }
  }
}
