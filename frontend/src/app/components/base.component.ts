import { MatSnackBar } from "@angular/material";

export class BaseComponent {
  constructor(public snackBar: MatSnackBar) {}

  showMsg(text: string): void {
    this.snackBar.open(text, "", {
      duration: 2000
    });
  }
}
