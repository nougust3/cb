import { Component, OnInit, Inject, Input, Output } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from "@angular/material";

import * as moment from "moment";

@Component({
  selector: "app-time-picker",
  templateUrl: "timePicker.component.html",
  styleUrls: ["./timePicker.component.scss"]
})
export class TimePickerComponent {
  isShowTime = false;
  isSelectHours = true;
  isSelectMinutes = false;

  @Output() selectedTime;

  currentDate;

  workHours = [];

  @Output() time = { hours: 0, minutes: 0 };
  @Output() res = "";

  constructor() {
    this.currentDate = moment().format("MMM Do YY");

    for (let hour = 0; hour < 24; hour++) {
      this.workHours.push(hour);
    }
  }

  setHoursRange(start, end) {
    this.workHours.length = 0;
    for (let hour = start; hour < end; hour++) {
      this.workHours.push(hour);
    }
  }

  onReset() {
    this.isShowTime = false;
    this.isSelectHours = true;
    this.isSelectMinutes = false;
  }

  onNextDate() {
    this.currentDate = moment(this.currentDate, "MMM Do YY")
      .add(1, "days")
      .format("MMM Do YY");
    moment(this.currentDate, "MMM Do YY").add(1, "days");
  }

  onPrevDate() {
    this.currentDate = moment(this.currentDate, "MMM Do YY")
      .subtract(1, "days")
      .format("MMM Do YY");
    moment(this.currentDate, "MMM Do YY").subtract(1, "days");
  }

  onSelectHour(hours) {
    this.time.hours = hours;
    this.isSelectHours = false;
    this.isSelectMinutes = true;
  }

  onSelectMinutes(minutes) {
    this.time.minutes = minutes;
    this.isSelectHours = false;
    this.isSelectMinutes = false;
    this.isShowTime = true;
    this.selectedTime = moment()
      .minutes(this.time.minutes)
      .hours(this.time.hours)
      .format("hh:mm");
  }

  getTime() {
    return moment(
      this.currentDate + " " + this.selectedTime,
      "MMM Do YY hh:mm"
    ).format("x");
  }
}
