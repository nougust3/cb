import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { TimePickerComponent } from "./timePicker.component";

describe("TimePickerComponent", () => {
  let component: TimePickerComponent;
  let fixture: ComponentFixture<TimePickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TimePickerComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimePickerComponent);
    component = fixture.componentInstance;
  });

  it("создается", () => {
    expect(component).toBeTruthy();
  });

  it("отображает часы", () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".hours")).toBeTruthy();
  });

  it("отображает рабочие часы", () => {
    component.setHoursRange(10, 12);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(component.workHours).toEqual([10, 11]);
  });

  it("отображает минуты", () => {
    component.onSelectHour(12);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".minutes")).toBeTruthy();
  });

  it("отображает выбранное время", () => {
    component.onSelectHour(12);
    component.onSelectMinutes(30);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".selected-time b").textContent).toBe(
      "12:30"
    );
  });

  it("сбрасывает выбранное время", () => {
    component.onSelectHour(12);
    component.onSelectMinutes(30);
    component.onReset();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".hours")).toBeTruthy();
  });
});
