export interface Task {
  id: string;
  service: string;
  customer: string;
  owner: string;
  comment: string;
  time: number;
}
