import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";

import { ReactiveFormsModule } from "@angular/forms";

import {
  MatSnackBar,
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatTabsModule,
  MatDialogModule,
  MatSnackBarModule,
  MatListModule,
  MatGridListModule,
  MatSliderModule
} from "@angular/material";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { MatCardModule } from "@angular/material/card";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { AuthModule } from "./modules/auth/auth.module";
import { HomeComponent } from "./components/home/home.component";

import { UserService } from "./services/user.service";
import { FormsModule } from "@angular/forms";
import { ProfileComponent } from "./components/profile/profile.component";
import { RouterModule, Routes } from "@angular/router";
import { ServicesComponent } from "./components/profile/services/services.component";
import { ServicesService } from "./services/services.service";
import { ServiceDialogComponent } from "./components/profile/services/dialog/dialog.component";
import { ProfileModule } from "./modules/profile.module";
import { TaskService } from "./services/task.service";
import { CustomerService } from "./services/customer.service";
import { AuthComponent } from "./modules/auth/auth.component";
import { TasksComponent } from "./components/profile/tasks/tasks.component";
import { CustomersComponent } from "./components/profile/customers/customers.component";
import { ServicesModule } from "./modules/services.module";
import { FacadeService } from "./services/facade.service";
import { BaseComponent } from "./components/base.component";

import { AuthInterceptor } from "./services/auth.interceptor";

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "auth", component: AuthComponent },
  { path: "profile", component: ProfileComponent }
];

@NgModule({
  declarations: [AppComponent, HomeComponent, ServiceDialogComponent],
  entryComponents: [
    TasksComponent,
    CustomersComponent,
    ServicesComponent,
    ServiceDialogComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatListModule,
    MatTabsModule,
    MatSnackBarModule,
    MatGridListModule,
    HttpClientModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    ProfileModule,
    ServicesModule,
    AuthModule,
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true })
  ],
  providers: [
    /* {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
