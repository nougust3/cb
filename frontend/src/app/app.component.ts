import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "./services/user.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "app";

  constructor(private service: UserService, private router: Router) {}

  ngOnInit() {
    if (this.service.isSigned()) {
      this.router.navigate(["/profile"]);
    } else {
      this.router.navigate(["/auth"]);
    }
  }
}
