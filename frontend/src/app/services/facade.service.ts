import { Injectable, Injector } from "@angular/core";
import { CustomerService } from "./customer.service";
import { TaskService } from "./task.service";
import { ServicesService } from "./services.service";
import { AuthService } from "./auth.service";
import { UserService } from "./user.service";

@Injectable()
export class FacadeService {
  private _customerService: CustomerService;
  public get customers(): CustomerService {
    if (!this._customerService) {
      this._customerService = this.injector.get(CustomerService);
    }
    return this._customerService;
  }

  private _taskService: TaskService;
  public get tasks(): TaskService {
    if (!this._taskService) {
      this._taskService = this.injector.get(TaskService);
    }
    return this._taskService;
  }

  private _servicesService: ServicesService;
  public get services(): ServicesService {
    if (!this._servicesService) {
      this._servicesService = this.injector.get(ServicesService);
    }
    return this._servicesService;
  }

  private _authService: AuthService;
  public get auth(): AuthService {
    if (!this._authService) {
      this._authService = this.injector.get(AuthService);
    }
    return this._authService;
  }

  private _userService: UserService;
  public get user(): UserService {
    if (!this._userService) {
      this._userService = this.injector.get(UserService);
    }
    return this._userService;
  }

  constructor(private injector: Injector) {}
}
