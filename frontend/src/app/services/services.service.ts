import { Injectable } from "@angular/core";
import { Service } from "./../models/service";
import { HttpClient } from "@angular/common/http";
import { ServicesRepository } from "./../data/services/services.repository";
import { ServicesDataStoreFactory } from "../data/services/services.store.factory";
import { ServicesCache } from "../data/services/services.cache";
import * as Rx from "rxjs";

@Injectable()
export class ServicesService {
  serviceRepository: ServicesRepository;

  constructor(private http: HttpClient) {
    let servicesCache = new ServicesCache();
    let servicesFactory = new ServicesDataStoreFactory(
      servicesCache,
      this.http
    );

    this.serviceRepository = new ServicesRepository(servicesFactory);
  }

  onDataChange(): Rx.Subject<any> {
    return this.serviceRepository.onDataChange();
  }

  getAll(): Service[] {
    return this.serviceRepository.getAll();
  }

  get(id: string): Service {
    return this.serviceRepository.get(id);
  }

  store(service): void {
    this.serviceRepository.store(service);
  }

  remove(id: string): void {
    this.serviceRepository.remove(id);
  }

  update(service: Service): void {
    this.serviceRepository.update(service);
  }
}
