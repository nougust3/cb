import { TestBed, inject } from "@angular/core/testing";
import {
  HttpTestingController,
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { ServicesService } from "./services.service";
import { UserService } from "./user.service";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";

describe("ServicesService", () => {
  let http;
  let service: ServicesService;
  let userService: UserService;
  const userServiceStub = {
    send: () => {}
  };
  const servicesList = [
    { id: "1", name: "service 1", description: "desc", price: 100 },
    { id: "2", name: "service 2", description: "desc", price: 200 },
    { id: "3", name: "service 3", description: "desc", price: 300 }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ServicesService,
        UserService,
        {
          provide: UserService,
          useValue: userServiceStub
        }
      ]
    });

    http = jasmine.createSpyObj("HttpClient", ["post", "get"]);
    //userService = new UserService(<any> http);
    // service = new ServicesService(<any> http, userService);
  });

  //afterEach(() => http.verify());

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should get services list", () => {
    http.get.and.returnValue(Observable.of(servicesList));

    /*service.getServices()
      .subscribe(services => {
        expect(services).toEqual(servicesList);
      });
*/
  });
});
