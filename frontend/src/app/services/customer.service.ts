import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import * as Rx from "rxjs";
import { endpoints } from "../../environments/environment";
import { Customer } from "../models/customer";
import { CustomersCache } from "../data/customers/customers.cache";
import { CustomersRepository } from "../data/customers/customers.repository";
import { CustomersDataStoreFactory } from "../data/customers/customers.store.factory";

@Injectable()
export class CustomerService {
  customerSubject: Rx.Subject<Customer[]>;
  customersRepository: CustomersRepository;

  constructor(private http: HttpClient) {
    let customersCache = new CustomersCache();
    let customersFactory = new CustomersDataStoreFactory(
      customersCache,
      this.http
    );

    this.customerSubject = new Rx.Subject();
    this.customersRepository = new CustomersRepository(customersFactory);

    this.onDataChange().subscribe(customers =>
      this.customerSubject.next(customers)
    );
  }

  onDataReady(): Rx.Subject<Customer[]> {
    return this.customerSubject;
  }

  onDataChange(): Rx.Subject<Customer[]> {
    return this.customersRepository.onDataChange();
  }

  getAll(): Customer[] {
    return this.customersRepository.getAll();
  }

  get(id: string): Customer {
    return this.customersRepository.get(id);
  }

  update(customer: Customer): void {
    this.customersRepository.update(customer);
  }

  store(customer: Customer): void {
    this.customersRepository.store(customer);
  }

  search(name: string): Customer[] {
    return this.customersRepository
      .getAll()
      .filter(customer => customer.name.includes(name));
  }
}
