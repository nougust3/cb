import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { endpoints } from "../../environments/environment";
import decode from "jwt-decode";
import "rxjs/add/operator/do";

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  public getToken(): string {
    return localStorage.getItem("token");
  }

  public setToken(token: string): void {
    localStorage.setItem("token", token);
  }

  public isSigned(): boolean {
    if (this.getToken()) {
      return true;
    } else {
      return false;
    }
  }

  public signin(email: string, password: string) {
    return this.http
      .post(endpoints.auth.signin, { email, password })
      .do(res => this.setToken);
  }

  public signup(email: string, password: string, name: string) {
    return this.http
      .post(endpoints.auth.signup, { email, password, name })
      .do(res => this.setToken);
  }

  public signout() {
    localStorage.removeItem("token");
  }
}
