import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { endpoints } from "../../environments/environment";
import { RequestService } from "../network/request.service";
import { HttpRequest } from "@angular/common/http";

@Injectable()
export class UserService {
  USER_URL = "http://localhost:8080/user/";
  private TOKEN_ITEM = "token";
  private EMAIL_ITEM = "email";

  private data: any;
  private email: string = localStorage.getItem(this.EMAIL_ITEM);

  workTime;

  constructor(
    private http: HttpClient,
    private router: Router,
    private requestService: RequestService
  ) {}

  getWorkTime() {
    return this.workTime;
  }

  setWorkTime(workTime) {
    this.workTime = workTime;

    this.requestService.push(endpoints.user.update, {
      workTime: this.workTime
    });
  }

  signIn(email: string, password: string, name: string): Observable<any> {
    this.email = email;
    return this.http
      .post(this.USER_URL + "signin", { email, password, name })
      .map(response => {
        if (response["success"]) {
          this.setUser(response["token"]);
        } else {
          return { status: response["success"], code: response["code"] };
        }
      });
  }

  signUp(email: string, password: string): Observable<any> {
    this.email = email;
    return this.http
      .post(this.USER_URL + "signup", { email, password })
      .map(response => {
        if (response["success"]) {
          this.setUser(response["token"]);
        } else {
          return { status: response["success"], code: response["code"] };
        }
      });
  }

  private setUser(token: string): void {
    localStorage.setItem(this.TOKEN_ITEM, token);
    localStorage.setItem(this.EMAIL_ITEM, this.email);
    this.router.navigate(["/profile"]);
  }

  private update(email: string, workTime): void {
    //return this.http.post();
  }

  getUser(): Observable<any> {
    return this.http
      .post(endpoints.user.get, { email: this.email })
      .map((response: Response) => {
        this.workTime = response["user"].workTime;
        return response["user"];
      });
  }

  getEmail() {
    return localStorage.getItem(this.EMAIL_ITEM);
  }

  getToken(): any {
    return localStorage.getItem(this.TOKEN_ITEM);
  }

  signOut() {
    localStorage.removeItem(this.TOKEN_ITEM);
    localStorage.removeItem(this.EMAIL_ITEM);
    this.router.navigate(["/auth"]);
  }

  isSigned() {
    return (
      localStorage.getItem(this.TOKEN_ITEM) !== "undefined" &&
      localStorage.getItem(this.TOKEN_ITEM) !== null
    );
  }
}
