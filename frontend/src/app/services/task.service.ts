import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import * as Rx from "rxjs";
import { endpoints } from "../../environments/environment";
import { Task } from "../models/task";
import { TasksCache } from "../data/tasks/tasks.cache";
import { TasksRepository } from "../data/tasks/tasks.repository";
import { TasksDataStoreFactory } from "../data/tasks/tasks.store.factory";

@Injectable()
export class TaskService {
  tasksSubject: Rx.Subject<Task[]>;
  tasksRepository: TasksRepository;

  private email: string = localStorage.getItem("email");

  constructor(private http: HttpClient) {
    let taksCache = new TasksCache();
    let tasksFactory = new TasksDataStoreFactory(taksCache, this.http);

    this.tasksSubject = new Rx.Subject();
    this.tasksRepository = new TasksRepository(tasksFactory);

    this.onDataChange()
      .first()
      .subscribe(tasks => this.tasksSubject.next(tasks));
  }

  onDataReady(): Rx.Subject<Task[]> {
    return this.tasksSubject;
  }

  onDataChange(): Rx.Subject<Task[]> {
    return this.tasksRepository.onDataChange();
  }

  getAll(): Task[] {
    let tasks = this.tasksRepository.getAll();
    return this.sort(tasks);
  }

  getByCustomer(id): Task[] {
    return this.tasksRepository.getAll().filter(task => task.customer === id);
  }

  store(taskData: any): any {
    this.tasksRepository.store({
      id: "",
      owner: "q",
      service: taskData.service,
      customer: taskData.customer,
      time: taskData.time,
      comment: ""
    });
  }

  remove(task: Task): void {
    this.tasksRepository.remove(task.id);
  }

  update(task: Task): void {
    this.tasksRepository.update(task);
  }

  private sort(tasks): Task[] {
    return tasks.sort((left, right) => (left.time <= right.time ? -1 : 1));
  }
}
