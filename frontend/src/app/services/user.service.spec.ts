import { TestBed, inject, fakeAsync } from "@angular/core/testing";

import { UserService } from "./user.service";
import {
  HttpTestingController,
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { HttpClient } from "@angular/common/http";

import { MockBackend } from "@angular/http/testing";
import { BaseRequestOptions, Http } from "@angular/http";

describe("UserService", () => {
  let service: UserService;
  let backend: MockBackend;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (back, options) => new Http(back, options),
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });

    backend = TestBed.get(MockBackend);
    service = TestBed.get(UserService);
  });

  describe("Auth success", () => {
    const response = null;

    /*it('Sign in should return true', () => {

      backend.connections.subscribe(connection => {
        connection.mockRespond(new Response({
          body: JSON.stringify(response)
        }));
      });

      service.signIn('q', 'q', '').subscribe(res => {
        expect(res).toEqual(true);
      });

    });

    it('Should store token and email', () => {

      backend.connections.subscribe(connection => {
        connection.mockRespond(new Response({
          body: JSON.stringify(response)
        }));
      });

      service.signIn('q', 'q', '').subscribe(res => {
        expect(service.getToken()).toEqual('a');
      });
    });*/
  });

  describe("Auth fail", () => {
    const response = {
      success: false
    };

    it(
      "Sign in should return false",
      fakeAsync(() => {
        backend.connections.subscribe(connection => {
          connection.mockRespond(
            new Response({
              body: JSON.stringify(response)
            })
          );
        });

        service.signIn("q", "q", "").subscribe(res => {
          expect(res).toEqual(false);
        });
      })
    );
  });
});
