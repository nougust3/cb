import { CustomerService } from "./../services/customer.service";
import { TaskService } from "./../services/task.service";
import { ServicesService } from "./../services/services.service";
import { UserService } from "./../services/user.service";
import { AuthService } from "./../services/auth.service";
import { FacadeService } from "./../services/facade.service";
import { RequestService } from "./../network/request.service";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    CustomerService,
    TaskService,
    ServicesService,
    UserService,
    AuthService,
    FacadeService,
    RequestService
  ]
})
export class ServicesModule {}
