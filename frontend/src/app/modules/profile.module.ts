import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { ProfileComponent } from "../components/profile/profile.component";
import {
  MatToolbarModule,
  MatTabsModule,
  MatListModule,
  MatCardMdImage,
  MatCardModule,
  MatButtonModule,
  MatSelectModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule
} from "@angular/material";
import { ServicesComponent } from "../components/profile/services/services.component";
import { TasksComponent } from "../components/profile/tasks/tasks.component";
import { CustomersComponent } from "../components/profile/customers/customers.component";
import { AppointDialogComponent } from "../components/profile/tasks/appointDialog/appointDialog.component";
import { ShiftDialogComponent } from "../components/profile/tasks/shiftDialog/shiftDialog.component";
import { EmptyComponent } from "../components/profile/empty.component";

import { TimePickerComponent } from "./../components/timePicker/timePicker.component";
import { AllAppointsDialogComponent } from "../components/profile/customers/allAppointsDialog/allAppointsDialog.component";
import { EditCustomerDialogComponent } from "../components/profile/customers/editDialog/editDialog.component";
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    MatToolbarModule,
    MatTabsModule,
    BrowserModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatProgressBarModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    ProfileComponent,
    CustomersComponent,
    ServicesComponent,
    TasksComponent,
    AppointDialogComponent,
    ShiftDialogComponent,
    TimePickerComponent,
    AllAppointsDialogComponent,
    EditCustomerDialogComponent,
    EmptyComponent
  ],
  entryComponents: [
    AppointDialogComponent,
    ShiftDialogComponent,
    AllAppointsDialogComponent,
    EditCustomerDialogComponent
  ],
  bootstrap: [ProfileComponent]
})
export class ProfileModule {}
