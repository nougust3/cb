import { Component, Output, OnInit } from "@angular/core";
import { UserService } from "./../../services/user.service";
import { FormControl, Validators } from "@angular/forms";
import { FacadeService } from "../../services/facade.service";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit {
  isNewUser = false;

  error = false;
  errorMessage = "";

  name = new FormControl("", [Validators.required]);
  email = new FormControl("", [Validators.required, Validators.email]);
  password = new FormControl("", [Validators.required]);

  constructor(private service: FacadeService) {}

  ngOnInit() {
    this.email.valueChanges.subscribe(val => {
      if (this.error) {
        this.error = false;
      }
    });
    this.email.valueChanges.subscribe(val => {
      if (this.error) {
        this.error = false;
      }
    });
  }

  onSign() {
    if (this.isNewUser) {
      this.signUp();
    } else {
      this.signIn();
    }
  }

  onSignChange() {
    this.isNewUser = !this.isNewUser;
    this.name.setValidators(this.isNewUser ? [Validators.required] : []);
  }

  signIn() {
    if (this.email.value !== "" && this.password.value !== "") {
      this.service.user
        .signIn(this.email.value, this.password.value, this.name.value)
        .subscribe(res => this.showError(res.code));
    }
  }

  signUp() {
    if (
      this.email.value !== "" &&
      this.password.value !== "" &&
      this.name.value !== ""
    ) {
      this.service.user
        .signUp(this.email.value, this.password.value)
        .subscribe(res => this.showError(res.code));
    }
  }

  private showError(code): void {
    this.error = true;

    switch (code) {
      case 1:
        this.errorMessage = "Ошибка сервера";
        break;
      case 2:
        this.errorMessage = "Пользователь уже существует";
        break;
      case 3:
        this.errorMessage = "Пользователь не найден";
        break;
      case 4:
        this.errorMessage = "Неверный пароль";
        break;
      default:
        this.errorMessage = "Неизвестная ошибка";
        break;
    }
  }
}
