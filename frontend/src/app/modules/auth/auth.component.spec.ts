import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AuthComponent } from "./auth.component";
import { PageObject } from "./../../../utils/pageObject";
import { DebugElement } from "@angular/core";

describe("AuthComponent | Страница авторизации", () => {
  class Page extends PageObject<AuthComponent> {
    get emailControl(): DebugElement {
      return this.getByAutomationId("email-control");
    }

    get authError(): DebugElement {
      return this.getByAutomationId("auth-error");
    }

    verifyErrorElement(element: DebugElement): boolean {
      return element.attributes["class"] === "error-message";
    }
  }

  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;
  let page: Page;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [AuthComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    page = new Page(fixture);
  });

  it("создается", () => {
    expect(component).toBeTruthy();
  });

  describe("Вход", () => {
    it("существует блок ошибки авторизации", () => {
      //expect(page.authError).toBe(true);
    });
  });

  describe("Регистрация", () => {
    it("существует блок ошибки авторизации", () => {
      //expect(page.authError).toBe(true);
    });
  });
});
