import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import {
  MatToolbarModule,
  MatTabsModule,
  MatListModule,
  MatCardMdImage,
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule
} from "@angular/material";
import { AuthComponent } from "./auth.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    MatToolbarModule,
    MatTabsModule,
    BrowserModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [AuthComponent],
  bootstrap: [AuthComponent]
})
export class AuthModule {}
