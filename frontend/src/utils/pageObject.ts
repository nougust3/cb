import { ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

export class PageObject<T> {

    constructor(protected fixture: ComponentFixture<T>) { }

    getByAutomationId(id: string): DebugElement {
      return this.fixture.debugElement.query(By.css(`[automation-id=${id}]`));
    }
    
}