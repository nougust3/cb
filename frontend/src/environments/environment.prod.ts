// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: "https://customer-base.herokuapp.com"
};

export const format = {
  date: "MMM Do YY",
  time: "hh:mm"
};

export const endpoints = {
  auth: {
    signin: environment.url + "/user/signin",
    signup: environment.url + "/user/signup"
  },
  task: {
    all: environment.url + "/task/all",
    update: environment.url + "/task/update",
    store: environment.url + "/task/create",
    remove: environment.url + "/task/remove"
  },
  customer: {
    all: environment.url + "/customer/all",
    store: environment.url + "/customer/create",
    update: environment.url + "/customer/update"
  },
  service: {
    all: environment.url + "/service/all",
    store: environment.url + "/service/create",
    update: environment.url + "/service/update"
  },
  user: {
    get: environment.url + "/user/get",
    update: environment.url + "/user/update"
  }
};
