const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');

module.exports = passport => {
  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: 'secret'
  };

  passport.use(new JwtStrategy(options, (payload, done) => {
    User.findOne({ email: payload.email }, (err, user) => {
      if (!err) {
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      } else {
        done(err, false);
      }
    });
  }));
};
